package name.markusreschke.splchatAOP;

public class Configuration {
	public static boolean AES = true;
	public static boolean DES = true;
	public static boolean GUI = false;
	public static boolean TUI = true;
	public static boolean SOUND = true;
	public static boolean ANTISPAM = true;
	public static boolean STATE = false;
	
	public static boolean isOk() {
		return (GUI ^ TUI) && (!STATE || GUI);
	}
}
