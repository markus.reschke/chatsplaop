package name.markusreschke.splchatAOP;

public class Configuration {
	public static boolean AES = false;
	public static boolean DES = true;
	public static boolean GUI = true;
	public static boolean TUI = false;
	public static boolean SOUND = false;
	public static boolean ANTISPAM = false;
	public static boolean STATE = false;
	
	public static boolean isOk() {
		return (GUI ^ TUI) && (!STATE || GUI);
	}
}
