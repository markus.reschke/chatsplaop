package name.markusreschke.splchatAOP.client;

public interface InputPrompter {
    String showInputPrompt(String message);
}
