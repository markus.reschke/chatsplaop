package name.markusreschke.splchatAOP.client;

import name.markusreschke.splchatAOP.*;

public interface UI extends InputPrompter{
    void setMessageTransportProvider(MessageTransportProvider mtp);
    void setServerConnectionData(ServerConnectionData sdc);
    void run();
    void newReceivedMessage(Message msg);
    ServerConnectionData promptForConnectionData();
    void showErrorMessage(String msg);
    String showInputPrompt(String message);
}
