package name.markusreschke.splchatAOP.client;

import name.markusreschke.splchatAOP.*;

public interface MessageTransportProvider {
    void sendMessage(Message msg);
}
