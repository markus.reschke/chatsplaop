package name.markusreschke.splchatAOP.client;

import name.markusreschke.splchatAOP.*;

import java.io.*;
import java.net.*;

public class ReceiverThread extends Thread {

    private ObjectInputStream inStream;
    
    public ReceiverThread(ObjectInputStream ois) {
        this.inStream = ois;
    }

    public void run() {
        while (!isInterrupted()) {
            Message msg = null;
            try {
                BytePacket p = (BytePacket) readPacket();
                msg = Utils.bytePacketToMessage(p);
            } catch (IOException e) {
            	if (!isInterrupted()) {
            		System.out.println("Can't read from server");
                	e.printStackTrace();
                	System.exit(1);
            	}
            }
            if (msg != null) {
                handleReceivedMessage(msg);
            }
            yield();
        }
    }
    
    public BytePacket readPacket() {
        try {
        	return (BytePacket) inStream.readObject();
        } catch (EOFException e) {
            return null;
        } catch (SocketException e) {
            return null;
        } catch (IOException e) {
            return null;
        } catch (ClassNotFoundException e) {
            return null;
        }
    }
    
    private void handleReceivedMessage(Message msg) {
    }
}
