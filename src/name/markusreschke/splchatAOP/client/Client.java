package name.markusreschke.splchatAOP.client;
import name.markusreschke.splchatAOP.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class Client implements MessageTransportProvider{
    private ReceiverThread rt;
	private ServerConnectionData sdc;
    private Socket s;
    private ObjectOutputStream outChannel;
    private ObjectInputStream inChannel;

    public Client() {
    	init();
        rt.start();
        run();
        quit();
    }
    
    private void init() {
        connect();
        rt = new ReceiverThread(inChannel);
    }
    
    private void error(String msg) {
    	System.out.println(msg);
    }

    private void connect() {
        try {
            s = new Socket(sdc.getHost(), sdc.getPort());
            outChannel = new ObjectOutputStream(s.getOutputStream());
            inChannel = new ObjectInputStream(s.getInputStream());
        } catch (IOException e) {
            error("Can't connect to server!");
            try {
                s.close();
            } catch (IOException e2) {
            } finally {
                System.exit(1);
            }
        }
    }

    public void sendMessage(Message msg) {
        try {
        	BytePacket p = Utils.messageToBytePacket(msg);
            sendPacket(p);
        } catch (IOException e) {
            e.printStackTrace();
            error("Couldn't send message!");
            return;
        }	
    }
    
    public boolean sendPacket(BytePacket p) {
        try {
            outChannel.writeObject(p);
            outChannel.flush();
        } catch (IOException e) {
            e.printStackTrace();
            error("Couldn't send message!");
            return false;
        }  
        return true;
    }

    public void quit() {
        rt.interrupt();
        try {
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
    
    private void run() {
    }
    
    public static void main(String[] args) {
    	if(!Configuration.isOk()) {
    		System.out.println("Invalid configuration!");
    		System.exit(1);
    	}
    	new Client();
    }
    
    public ServerConnectionData getSdc() {
		return sdc;
	}

	public void setSdc(ServerConnectionData sdc) {
		this.sdc = sdc;
	}
}
