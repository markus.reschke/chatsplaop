package name.markusreschke.splchatAOP;

import java.io.*;
import java.util.*;

public class Message implements Serializable {
    private static final long serialVersionUID = 8619958701744010540L;

    private String message;
    private String clientId;

    public Message(String message, String clientId) {
        this.message = message;
        this.clientId = clientId;
    }

    public String toString() {
        return toSimpleDisplayString();
    }

    public String toSimpleDisplayString() {
        String displayText = message;
        return clientId + ": " + displayText;
    }

    public String getClientId() {
        return clientId;
    }

    public String getMessage() {
        return message;
    }

}
