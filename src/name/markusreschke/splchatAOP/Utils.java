package name.markusreschke.splchatAOP;

import java.io.*;

public class Utils {
    public static Message bytePacketToMessage(BytePacket p) throws IOException {
    	if (p == null) return null;
        ByteArrayInputStream bis = new ByteArrayInputStream(p.getData());
        ObjectInputStream ois = new ObjectInputStream(bis);
        try {
            return (Message) ois.readObject();
        } catch (ClassNotFoundException e) {
            //Sollte nicht auftreten
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public static BytePacket messageToBytePacket(Message msg) throws IOException {
    	if (msg == null) return null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(bos);
        os.writeObject(msg);
        BytePacket p = new BytePacket(bos.toByteArray());
        return p;
    }

    public static BytePacket stringToPacket(String s) {
        return new BytePacket(s.getBytes());
    }

    public static String packetToString(BytePacket p) {
        return new String(p.getData());
    }
}
