package name.markusreschke.splchatAOP;

public class Configuration {
	public static boolean AES = true;
	public static boolean DES = true;
	public static boolean GUI = true;
	public static boolean TUI = false;
	public static boolean SOUND = true;
	public static boolean ANTISPAM = true;
	public static boolean STATE = true;
	
	public static boolean isOk() {
		return (GUI ^ TUI) && (!STATE || GUI);
	}
}
