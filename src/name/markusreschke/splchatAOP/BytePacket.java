package name.markusreschke.splchatAOP;

import java.io.*;

public class BytePacket implements Serializable {
    private static final long serialVersionUID = -3791976064142541252L;
    private byte[] data;

    public BytePacket(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
