package name.markusreschke.splchatAOP.aspects;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;
import name.markusreschke.splchatAOP.server.*;

public aspect DES {

	public class DESEncryption {

	    private byte[] keyDES = {'d', 'g', '5', '4', 'c', 'j', 'g', 's', 't'};
	    private SecretKey desKey;

	    public DESEncryption() {
	        try {
	            KeySpec desSpec = new DESKeySpec(keyDES);
	            desKey = SecretKeyFactory.getInstance("DES").generateSecret(desSpec);
	        } catch (InvalidKeySpecException e) {
	            //Shouldn't happen because of static key
	            e.printStackTrace();
	        } catch (InvalidKeyException e) {
	            //Shouldn't happen because of static key
	            e.printStackTrace();
	        } catch (NoSuchAlgorithmException e) {
	            //Shouldn't happen because of static algorithm included with JDK
	            e.printStackTrace();
	        }
	    }
	    
	    public BytePacket doModification(BytePacket p) {
	    	if (p == null) return null;
	    	p.setData(doModification(p.getData()));
	    	return p;
	    }

	    public byte[] doModification(byte[] data) {
	        byte[] encrypted = null;
	        try {
	            Cipher cipherDES = Cipher.getInstance("DES/ECB/PKCS5Padding");
	            cipherDES.init(Cipher.ENCRYPT_MODE, desKey);
	            encrypted = cipherDES.doFinal(data);
	        } catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        } catch (NoSuchPaddingException e) {
	            e.printStackTrace();
	        } catch (IllegalBlockSizeException e) {
	            e.printStackTrace();
	        } catch (BadPaddingException e) {
	            e.printStackTrace();
	        } catch (InvalidKeyException e) {
	            e.printStackTrace();
	        }
	        return encrypted;
	    }
	    
	    public BytePacket reverseModification(BytePacket p) {
	    	if (p == null) return null;
	    	p.setData(reverseModification(p.getData()));
	    	return p;
	    }

	    public byte[] reverseModification(byte[] data) {
	        byte[] decrypted = null;
	        try {
	            Cipher cipherDES = Cipher.getInstance("DES/ECB/PKCS5Padding");
	            cipherDES.init(Cipher.DECRYPT_MODE, desKey);
	            decrypted = cipherDES.doFinal(data);
	        } catch (NoSuchAlgorithmException e) {
	            e.printStackTrace();
	        } catch (NoSuchPaddingException e) {
	            e.printStackTrace();
	        } catch (IllegalBlockSizeException e) {
	            e.printStackTrace();
	        } catch (BadPaddingException e) {
	            e.printStackTrace();
	        } catch (InvalidKeyException e) {
	            e.printStackTrace();
	        }
	        return decrypted;
	    }
	}
	
	DESEncryption des = new DESEncryption();
	
	pointcut sendPkg(BytePacket pkg): 
		execution(boolean *.sendPacket(BytePacket)) && args(pkg) && 
		if(Configuration.DES);
	pointcut readPkg(): execution(BytePacket *.readPacket()) && 
		if(Configuration.DES);
	
	boolean around(BytePacket pkg) : sendPkg(pkg) {
    	pkg = des.doModification(pkg);
    	return proceed(pkg);
	}
	
	BytePacket around() : readPkg() {
		BytePacket pkg = proceed();
		return des.reverseModification(pkg);
	} 

}