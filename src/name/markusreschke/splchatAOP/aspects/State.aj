package name.markusreschke.splchatAOP.aspects;

import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;
import name.markusreschke.splchatAOP.server.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public aspect State {
	private int ClientHandlingThread.state;
	private boolean Message.changeState;
	private int Message.state;
	private GUI gui = GUI.aspectOf();
	
	private final static int ONLINE = 0;
	private final static int BUSY = 1;
	private final static String[] stateNames = {"Online", "Busy"};
	
	private JComboBox stateComboBox;
	
	pointcut guiRun(): execution(void GUI.GUIImpl.run()) && 
	if(Configuration.STATE) ;
	
	before() : guiRun() {
        stateComboBox = new JComboBox(stateNames);
        stateComboBox.setSelectedIndex(ONLINE);
        stateComboBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
			       if (e.getStateChange() == ItemEvent.SELECTED) {
	    	   			sendChangeStateMsg();
			        }
			}
		});
		gui.gui.addComponent(stateComboBox);
		sendChangeStateMsg();
	}
	
	private void sendChangeStateMsg() {
 	   int state = stateComboBox.getSelectedIndex();
 	   System.out.println("State: "+stateNames[state]);
	   Message msg = new Message("",gui.gui.getSdc().getClientId());
	   msg.changeState = true;
	   msg.state = state;
	   gui.gui.getMtp().sendMessage(msg);
	}
	
	pointcut broadcast(Message msg, ClientHandlingThread t): 
		call(void Server.broadcast(Message)) && args(msg) && 
		if(Configuration.STATE) && this(t);

	void around(Message msg, ClientHandlingThread t) : broadcast(msg, t) {
		if(!msg.changeState) {
			msg.state = t.state;
			proceed(msg,t);
		} else {
			t.state = msg.state;
			System.out.println("State for " + msg.getClientId() + ": " +
					stateNames[t.state]);
		}
	}
	
	pointcut messageFormat(Message msg): 
		execution(String Message.toSimpleDisplayString()) && this(msg) && 
		if(Configuration.STATE);
	
	String around(Message msg): messageFormat(msg) {        
		String displayText = proceed(msg);
		int splitPos = displayText.indexOf(":")+1;
        displayText = displayText.substring(splitPos);
        
        return msg.getClientId() + " (" + stateNames[msg.state] + "): " + displayText;

	}

}
