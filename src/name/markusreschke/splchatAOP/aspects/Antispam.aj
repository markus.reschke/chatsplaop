package name.markusreschke.splchatAOP.aspects;

import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.server.*;

public aspect Antispam {

	public class SpamFilter {

		String[] wordList = { "Casino", "Gold", "Cheep", "OEM Software" };

		public Message processMessage(Message msg) {
			for (int i = 0; i < wordList.length; ++i) {
				if (msg.getMessage().contains(wordList[i])) {
					return null;
				}
			}
			return msg;
		}

		public boolean isSpam(Message msg) {
			return processMessage(msg) == null;
		}
	}
	
	SpamFilter filter = new SpamFilter();

	pointcut broadcast(Message msg): 
		execution(void Server.broadcast(Message)) && args(msg) && 
		if(Configuration.ANTISPAM);

	void around(Message msg) : broadcast(msg) {
		if(!filter.isSpam(msg)) {
			proceed(msg);
		}
	}
}
