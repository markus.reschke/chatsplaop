package name.markusreschke.splchatAOP.aspects;

import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public aspect GUI {

	public class ChatWindow extends JDialog {

		private JTextArea messagesReceived;
		private JTextField messageTf;
		JPanel sendingArea;
		private String clientId;
		MessageTransportProvider mtp;
		private int extensionLayoutOffset = 0;

		public ChatWindow() {
			init();
		}

		private void init() {
			setModal(true);
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
			setSize(500, 400);
			getContentPane().setLayout(new BorderLayout());

			JPanel receivingArea = new JPanel(new BorderLayout());
			messagesReceived = new JTextArea();
			messagesReceived.setEditable(false);
			JScrollPane sp = new JScrollPane(messagesReceived);
			receivingArea.add(sp, BorderLayout.CENTER);

			sendingArea = new JPanel(new GridBagLayout());
			messageTf = new JTextField(160);
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.gridwidth = 8;
			gbc.weightx = 0.8;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			sendingArea.add(messageTf, gbc);

			JButton sendButton = new JButton("Send");
			gbc = new GridBagConstraints();
			gbc.gridx = 10;
			gbc.gridy = 0;
			gbc.weightx = 0.1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			sendingArea.add(sendButton, gbc);

			getContentPane().add(receivingArea, BorderLayout.CENTER);
			getContentPane().add(sendingArea, BorderLayout.SOUTH);

			sendButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					sendMessage();
				}
			});
		}

		public void setClientId(String clientId) {
			this.clientId = clientId;
			setTitle("SPLChat: " + clientId);
		}

		public void setMtp(MessageTransportProvider mtp) {
			this.mtp = mtp;
		}

		public void addComponent(Component c) {
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = extensionLayoutOffset;
			gbc.gridy = 1;
			gbc.gridwidth = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			sendingArea.add(c, gbc);
			extensionLayoutOffset++;
			sendingArea.revalidate();
			revalidate();
		}

		private Message populateMessage() {
			return populateMessage(null);
		}

		private Message populateMessage(Message msg) {
			if (msg == null) {
				msg = new Message(messageTf.getText(), clientId);
			}
			return msg;
		}

		private void sendMessage() {
			Message msg = populateMessage();
			mtp.sendMessage(msg);
		}

		public void showMessage(Message msg) {
			this.messagesReceived.append(msg.toSimpleDisplayString() + "\n");
		}

	}

	public class LoginForm extends JDialog {
		private JLabel hostLabel = new JLabel("Host");
		private JLabel portLabel = new JLabel("Port");
		private JLabel clientIdLabel = new JLabel("Client ID");
		private JTextField hostTf = new JTextField(50);
		private JTextField portTf = new JTextField(50);
		private JTextField clientIdTf = new JTextField(50);
		private JButton okButton = new JButton("OK");
		private ServerConnectionData sdc;

		public LoginForm() {
			super((JDialog) null, "Login", true);
			setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			getContentPane().setLayout(new GridBagLayout());
			setResizable(false);

			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 0;
			gbc.gridy = 0;
			getContentPane().add(hostLabel, gbc);

			gbc = new GridBagConstraints();
			gbc.weightx = 0.8;
			gbc.gridx = 1;
			gbc.gridy = 0;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			getContentPane().add(hostTf, gbc);
			hostTf.setText("localhost");

			gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 0;
			gbc.gridy = 1;
			getContentPane().add(portLabel, gbc);

			gbc = new GridBagConstraints();
			gbc.weightx = 0.8;
			gbc.gridx = 1;
			gbc.gridy = 1;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			getContentPane().add(portTf, gbc);
			portTf.setText("50500");

			gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 0;
			gbc.gridy = 2;
			getContentPane().add(clientIdLabel, gbc);

			gbc = new GridBagConstraints();
			gbc.weightx = 0.8;
			gbc.gridx = 1;
			gbc.gridy = 2;
			gbc.fill = GridBagConstraints.HORIZONTAL;
			getContentPane().add(clientIdTf, gbc);
			clientIdTf.setText("TestNutzer");

			gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.BOTH;
			gbc.gridx = 0;
			gbc.gridy = 4;
			gbc.gridwidth = 2;
			gbc.weighty = 1;
			getContentPane().add(okButton, gbc);

			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					sdc = null;
					if (!validateInput()) {
						JOptionPane.showMessageDialog(LoginForm.this,
								"There are errors in the entered data.",
								"Wrong Input", JOptionPane.ERROR_MESSAGE);
					} else {
						sdc = new ServerConnectionData();
						sdc.setHost(hostTf.getText());
						sdc.setPort(Integer.valueOf(portTf.getText()));
						sdc.setClientId(clientIdTf.getText());
						LoginForm.this.dispose();
					}
				}
			});

			setSize(300, 100);
			setVisible(true);

		}

		private boolean validateInput() {
			int port = -1;
			if (portTf.getText().isEmpty())
				return false;
			try {
				port = Integer.parseInt(portTf.getText());
			} catch (NumberFormatException nfe) {
				return false;
			}
			if (port < 0 || port > 65535)
				return false;
			return !(clientIdTf.getText().isEmpty() || hostTf.getText()
					.isEmpty()); // Empty password field is OK
		}

		public ServerConnectionData getServerConnectionData() {
			return this.sdc;
		}
	}

	public class PromptDialog extends JDialog {
		private JLabel dataLabel;
		private JTextField dataTf = new JTextField(50);
		private JButton okButton = new JButton("OK");
		private String data = "";

		public PromptDialog(String msg) {
			super((JDialog) null, msg, true);
			setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			getContentPane().setLayout(new GridBagLayout());
			setResizable(false);

			dataLabel = new JLabel(msg);
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 0;
			gbc.gridy = 0;
			getContentPane().add(dataLabel, gbc);

			gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 1;
			gbc.gridy = 0;
			getContentPane().add(dataTf, gbc);

			gbc = new GridBagConstraints();
			gbc.fill = GridBagConstraints.HORIZONTAL;
			gbc.weightx = 0.2;
			gbc.gridx = 0;
			gbc.gridy = 1;
			getContentPane().add(okButton, gbc);

			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent actionEvent) {
					PromptDialog.this.data = PromptDialog.this.dataTf.getText();
					PromptDialog.this.dispose();
				}
			});

			setSize(300, 100);
			setVisible(true);
		}

		public String getData() {
			return data;
		}
	}

	public class GUIImpl implements UI {

		private MessageTransportProvider mtp;
		private ServerConnectionData sdc;
		private ChatWindow cw = new ChatWindow();

		public ServerConnectionData getSdc() {
			return sdc;
		}		
		
		public void setMessageTransportProvider(MessageTransportProvider mtp) {
			this.mtp = mtp;
			cw.setMtp(mtp);
		}
		
		public MessageTransportProvider getMtp() {
			return this.mtp;
		}

		public void setServerConnectionData(ServerConnectionData sdc) {
			this.sdc = sdc;
			cw.setClientId(sdc.getClientId());
		}

		public void run() {
			cw.setVisible(true);
		}

		public void newReceivedMessage(Message msg) {
			cw.showMessage(msg);
		}

		public ServerConnectionData promptForConnectionData() {
			LoginForm lf = new LoginForm();
			return lf.getServerConnectionData();
		}

		public void showErrorMessage(String msg) {
			JOptionPane.showMessageDialog(null, msg, "Error",
					JOptionPane.ERROR_MESSAGE);
		}

		public String showInputPrompt(String message) {
			PromptDialog d = new PromptDialog(message);
			return d.getData();
		}

		public void addComponent(Component c) {
			cw.addComponent(c);
		}
	}

	pointcut init(Client client) : execution(void Client.init()) && this(client) && 
		if(Configuration.GUI);

	pointcut errorMsg(String msg) : 
		execution(void Client.error(String)) && args(msg)&& 
		if(Configuration.GUI);

	pointcut run() : execution(void Client.run()) && 
		if(Configuration.GUI);

	pointcut handleReceivedMessage(Message msg) : 
		execution(void ReceiverThread.handleReceivedMessage(Message)) && args(msg) && 
		if(Configuration.GUI);

	GUIImpl gui = new GUIImpl();

	before(Client client) : init(client) {
		gui.setMessageTransportProvider(client);
		ServerConnectionData sdc = gui.promptForConnectionData();
		client.setSdc(sdc);
		gui.setServerConnectionData(sdc);
	}

	void around(String msg) : errorMsg(msg) {
		System.out.println(msg);
	}

	void around() : run() {
		gui.run();
	}

	after(Message msg) :  handleReceivedMessage(msg) {
		gui.newReceivedMessage(msg);
	}
}
