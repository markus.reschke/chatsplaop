package name.markusreschke.splchatAOP.aspects;

import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;

import java.io.*;
import java.net.*;
import java.util.*;


public aspect TUI {
	public class TUIImpl implements UI {

	    private ServerConnectionData sdc;
	    private Scanner sc;
	    MessageTransportProvider mtp;

	    public TUIImpl() {
	        sc = new Scanner(System.in);
	    }

	    public void setMessageTransportProvider(MessageTransportProvider mtp) {
	        this.mtp = mtp;
	    }

	    public void setServerConnectionData(ServerConnectionData sdc) {
	        this.sdc = sdc;
	    }

	    public void run() {
	        String line;
	        while (!(line = sc.nextLine()).equals("")) {
	            Message msg = new Message(line, sdc.getClientId());
	            mtp.sendMessage(msg);
	        }
	    }

	    public void newReceivedMessage(Message msg) {
	        System.out.println(msg.toSimpleDisplayString());
	    }

	    public ServerConnectionData promptForConnectionData() {

	        System.out.print("Enter port (empty line = default port): ");
	        String port = sc.nextLine().trim();
	        System.out.print("Enter host (empty line = default host): ");
	        String host = sc.nextLine().trim();
	        System.out.print("Enter username (empty line = default username): ");
	        String clientId = sc.nextLine().trim();
	        if (port.isEmpty()) port = "50500";
	        if (host.isEmpty()) host = "localhost";
	        if (clientId.isEmpty()) clientId = "qwertzUser";

	        ServerConnectionData sdc = new ServerConnectionData();
	        sdc.setHost(host);
	        sdc.setClientId(clientId);
	        try {
	            sdc.setPort(Integer.valueOf(port));
	        } catch (NumberFormatException e) {
	            System.out.println("Port is not numeric");
	            return null;
	        }
	        return sdc;
	    }

	    public void showErrorMessage(String msg) {
	        System.err.println(msg);
	    }

	    public String showInputPrompt(String message) {
	        System.out.print(message+": ");
	        String data = sc.nextLine();
	        return data;
	    }

	}
	
	pointcut init(Client client) : execution(void Client.init()) && this(client) && 
		if(Configuration.TUI);
	pointcut errorMsg(String msg) : 
		execution(void Client.error(String)) && args(msg) && 
		if(Configuration.TUI);
	pointcut run() : execution(void Client.run()) && 
		if(Configuration.TUI);
	pointcut handleReceivedMessage(Message msg) : 
		execution(void ReceiverThread.handleReceivedMessage(Message)) && args(msg) && 
		if(Configuration.TUI);
	
	TUIImpl tui = new TUIImpl();
	
	before(Client client) : init(client) {
		tui.setMessageTransportProvider(client);
		ServerConnectionData sdc = tui.promptForConnectionData();
		client.setSdc(sdc);
		tui.setServerConnectionData(sdc);
	}
	
	void around(String msg) : errorMsg(msg) {
		System.out.println(msg);
	}
	
	void around() : run() {
		tui.run();
	}
	
	after(Message msg) :  handleReceivedMessage(msg) {
		tui.newReceivedMessage(msg);
	}
}
