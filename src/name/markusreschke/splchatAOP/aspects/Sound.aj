package name.markusreschke.splchatAOP.aspects;

import java.awt.Toolkit;

import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;

public aspect Sound {

	pointcut handleReceivedMessage() : 
		execution(void ReceiverThread.handleReceivedMessage(Message)) && 
		if(Configuration.SOUND);
	
	after() : handleReceivedMessage() {
		Toolkit.getDefaultToolkit().beep();
		System.out.println("Beep!");
	}
	
}
