package name.markusreschke.splchatAOP.aspects;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import name.markusreschke.splchatAOP.*;
import name.markusreschke.splchatAOP.client.*;
import name.markusreschke.splchatAOP.server.*;

public aspect AES {
	
	declare precedence: DES, AES;

	public class AESEncryption {
		private byte[] keyAES = { 'd', 'h', 'g', '6', '5', '4', 'c', 'j', '5',
				'4', 'c', 'j', '8', 'g', 's', 't' };
		private SecretKey aesKey;

		public AESEncryption() {
			aesKey = new SecretKeySpec(keyAES, "AES");
		}

		public BytePacket doModification(BytePacket p) {
			if (p == null)
				return null;
			p.setData(doModification(p.getData()));
			return p;
		}

		public byte[] doModification(byte[] data) {
			Cipher cipherAES = null;
			byte[] encrypted = null;
			try {
				cipherAES = Cipher.getInstance("AES/ECB/PKCS5Padding");
				cipherAES.init(Cipher.ENCRYPT_MODE, aesKey);
				encrypted = cipherAES.doFinal(data);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			return encrypted;
		}

		public BytePacket reverseModification(BytePacket p) {
			if (p == null)
				return null;
			p.setData(reverseModification(p.getData()));
			return p;
		}

		public byte[] reverseModification(byte[] data) {
			Cipher cipherAES = null;
			byte[] decrypted = null;
			try {
				cipherAES = Cipher.getInstance("AES/ECB/PKCS5Padding");
				cipherAES.init(Cipher.DECRYPT_MODE, aesKey);
				decrypted = cipherAES.doFinal(data);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			}
			return decrypted;
		}
	}
	
	AESEncryption aes = new AESEncryption();
	
	pointcut sendPkg(BytePacket pkg): 
		execution(boolean *.sendPacket(BytePacket)) && args(pkg) && 
		if(Configuration.AES);
	pointcut readPkg(): execution(BytePacket *.readPacket()) && 
		if(Configuration.AES);
	
	boolean around(BytePacket pkg) : sendPkg(pkg) {
    	pkg = aes.doModification(pkg);
    	return proceed(pkg);
	}
	
	BytePacket around() : readPkg() {
		BytePacket pkg = proceed();
		return aes.reverseModification(pkg);
	} 

}
