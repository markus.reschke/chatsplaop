package name.markusreschke.splchatAOP.server;

import name.markusreschke.splchatAOP.*;

import java.io.*;
import java.net.*;
import java.util.*;

public class Server {
    private List clients;
    private ServerSocket ss;
    private ObjectOutputStream outChannel;
    private ObjectInputStream inChannel;
    
    public static void main(String[] args) {
    	if(!Configuration.isOk()) {
    		System.out.println("Invalid configuration!");
    		System.exit(1);
    	}
        if (args.length >= 2) {
            printHelp();
            System.exit(0);
        }
        int port = 50500;
        try {
            if (args.length == 1) {
                port = Integer.parseInt(args[4]);
            }
            if (port < 0 || port > 65535) System.out.println("Ung�ltige Portangabe");
        } catch (NumberFormatException e) {
            System.out.println("Port ist kein numerischer Wert");
            System.exit(1);
        }
        new Server(port);
    }

    private static void printHelp() {
        System.out.println("Benutzung: java -jar ChatServer.jar [Port]");
        System.out.println("Portangabe ist optional (Standardwert: 50500)");
    }

    public Server(int port) {
        clients = new ArrayList();
        try {
            ss = new ServerSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        run();
    }

    public void run() {
        connectionLoop:
        while (true) {
            try {
                Socket s = ss.accept();
                try {
                    this.outChannel = new ObjectOutputStream(s.getOutputStream());
                    this.inChannel = new ObjectInputStream(s.getInputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
                ClientHandlingThread cht = new ClientHandlingThread(inChannel, outChannel, this);
                registerClient(cht);
                try {
                    Thread.sleep(0);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    public void registerClient(ClientHandlingThread client) {
        synchronized (clients) {
            System.out.println("New Client");
            clients.add(client);
        }
        client.start();
    }

    public void removeClient(ClientHandlingThread client) {
        synchronized (clients) {
            System.out.println("Lost Client");
            clients.remove(client);
        }
    }

    public void broadcast(Message msg) {
        synchronized (clients) {
            Iterator it = clients.iterator();
            while (it.hasNext()) {
                boolean success = ((ClientHandlingThread) it.next()).sendMessage(msg);
                if (!success) {
                    it.remove();
                }
            }
        }
    }
}
