package name.markusreschke.splchatAOP.server;

import name.markusreschke.splchatAOP.*;

import java.io.*;
import java.net.*;

public class ClientHandlingThread extends Thread {
    private Socket socket;
    private Server server;
    private ObjectOutputStream outChannel;
    private ObjectInputStream inChannel;

    public ClientHandlingThread(ObjectInputStream ois, ObjectOutputStream oos, Server server) {
        this.server = server;
        inChannel = ois;
        outChannel = oos;
    }

    public synchronized boolean sendMessage(Message msg) {
    	try {
    		BytePacket p = Utils.messageToBytePacket(msg);
    		return sendPacket(p);
    	} catch (IOException e) {
            e.printStackTrace();
            return false;
    	}
    }
    
    public synchronized boolean sendPacket(BytePacket p) {
        try {
            outChannel.writeObject(p);
            outChannel.flush();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        };
        return true;    	
    }

    public void run() {
        while (!isInterrupted()) {
            Message msg = null;
            BytePacket p = readPacket();
            if (p == null) return;
            try {
            	msg = Utils.bytePacketToMessage(p);
            } catch (IOException e) {
            	e.printStackTrace();
            	continue;
            }
            server.broadcast(msg);
            yield();
        }
        System.out.println("Thread interrupted");
    }
    
    public BytePacket readPacket() {
        try {
        	return (BytePacket) inChannel.readObject();
        } catch (EOFException e) {
            server.removeClient(this);
            return null;
        } catch (SocketException e) {
            server.removeClient(this);
            return null;
        } catch (IOException e) {
            server.removeClient(this);
            return null;
        } catch (ClassNotFoundException e) {
            server.removeClient(this);
            return null;
        }
    }
}